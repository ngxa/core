# @ngxa/core

Angular core library.

[![npm](https://img.shields.io/npm/v/@ngxa/core.svg)](https://www.npmjs.com/package/@ngxa/core)
[![NpmLicense](https://img.shields.io/npm/l/@ngxa/core.svg)](https://gitlab.com/ngxa/core/blob/master/LICENSE.md)
[![coverage report](https://gitlab.com/ngxa/core/badges/master/coverage.svg)](https://gitlab.com/ngxa/core/commits/master)
[![sonarqube](https://sonarcloud.io/api/project_badges/measure?project=ngxa%3Acore&metric=alert_status)](https://sonarcloud.io/dashboard?id=ngxa%3Acore)
[![documentation](https://img.shields.io/badge/web-docs-orange.svg)](http://ngxa.gitlab.io/core)
[![coverage](https://img.shields.io/badge/web-coverage-orange.svg)](http://ngxa.gitlab.io/core/coverage/)
[![tests](https://img.shields.io/badge/web-tests-orange.svg)](http://ngxa.gitlab.io/core/coverage/unit-tests-report.html)

## Installation

You can install @ngxa/core, and all its dependencies, using npm.

```shell
npm install --save @ngxa/core
```
